package ws;

public class IProductProxy implements ws.IProduct {
  private String _endpoint = null;
  private ws.IProduct iProduct = null;
  
  public IProductProxy() {
    _initIProductProxy();
  }
  
  public IProductProxy(String endpoint) {
    _endpoint = endpoint;
    _initIProductProxy();
  }
  
  private void _initIProductProxy() {
    try {
      iProduct = (new ws.ProductImplServiceLocator()).getProductImplPort();
      if (iProduct != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iProduct)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iProduct)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iProduct != null)
      ((javax.xml.rpc.Stub)iProduct)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.IProduct getIProduct() {
    if (iProduct == null)
      _initIProductProxy();
    return iProduct;
  }
  
  public ws.Product[] findThreeBestSales() throws java.rmi.RemoteException{
    if (iProduct == null)
      _initIProductProxy();
    return iProduct.findThreeBestSales();
  }
  
  public ws.Product findById(int arg0) throws java.rmi.RemoteException{
    if (iProduct == null)
      _initIProductProxy();
    return iProduct.findById(arg0);
  }
  
  public ws.Product[] findAll() throws java.rmi.RemoteException{
    if (iProduct == null)
      _initIProductProxy();
    return iProduct.findAll();
  }
  
  
}