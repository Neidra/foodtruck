package main;

import java.rmi.RemoteException;
import java.util.*;

import javax.xml.rpc.ServiceException;

import ws.IProduct;
import ws.Product;
import ws.ProductImplService;
import ws.ProductImplServiceLocator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
            ProductImplService ProductsImplementsService = new ProductImplServiceLocator();
            IProduct Product = ProductsImplementsService.getProductImplPort();
            
            ws.Product[] ps = Product.findThreeBestSales();
            for (Product p : ps) {
				System.out.println("Nombre de ventes :" +p.getNbVente() + ", " + p.getName() + ", " + p.getPrice() + "�, note : " + p.getRank());
			}

		} catch (RemoteException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
		
	}

}
