package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import entities.Product;

public class DAOProduct implements DAO<Product, Integer> {
	
	static private String DBHOST = "jdbc:mysql://localhost:3306/foodtruck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";

	private List<Product> ps = new ArrayList<Product>();
	
	
	public Product findById(Integer uId) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			
			PreparedStatement ps=conn.prepareStatement("select * from product where id=?");
			ps.setInt(1,uId);
			ResultSet rs=ps.executeQuery();
			Product p=null;
			while(rs.next())
			{
				p=new Product(rs.getInt("id"),rs.getString("name"),rs.getString("description"), rs.getString("availability"),rs.getDouble("price"), rs.getInt("nb_vente"), rs.getInt("rank"));
			}
			rs.close();
			ps.close();
			conn.close();
			return p;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<Product> findAll() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			PreparedStatement ps=conn.prepareStatement("select * from product");
			ResultSet rs=ps.executeQuery();
			List<Product> liste=new ArrayList();
			while(rs.next())
			{
				Product p=new Product(rs.getInt("id"),rs.getString("name"),rs.getString("description"), rs.getString("availability"),rs.getDouble("price"), rs.getInt("nb_vente"), rs.getInt("rank"));
				liste.add(p);
			}
			rs.close();
			ps.close();
			conn.close();
			return liste;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<Product> findThreeBestSales() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			PreparedStatement ps=conn.prepareStatement("select * from product ORDER BY nb_vente DESC LIMIT 3");
			ResultSet rs=ps.executeQuery();
			List<Product> liste=new ArrayList();
			while(rs.next())
			{
				Product p=new Product(rs.getInt("id"),rs.getString("name"),rs.getString("description"), rs.getString("availability"),rs.getDouble("price"), rs.getInt("nb_vente"), rs.getInt("rank"));
				liste.add(p);
			}
			rs.close();
			ps.close();
			conn.close();
			return liste;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}


}