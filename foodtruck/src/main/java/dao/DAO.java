package dao;
import java.util.List;

import entities.Product;

public interface DAO<T, K> {

	T findById(K uId);
	List<T> findAll();
}
