package entities;

public class News {

	private int id;
	private String titre, texte, img;
	
	public News(int id, String titre, String texte, String img  ) {
		this.id = id;
		this.img = img;
		this.titre = titre;
		this.texte = texte;
	}
	
	public News(String titre, String texte, String img ) {
		this.img = img;
		this.titre = titre;
		this.texte = texte;
	}
	
	public News() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", titre=" + titre + ", texte=" + texte + ", img=" + img + "]";
	}
	
	
	
	
	
	
	
}
