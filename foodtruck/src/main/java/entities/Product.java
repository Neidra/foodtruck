package entities;

public class Product {

	
	private int id;
	private String name, description, availability;
	private double price;
	private int nbVente, rank;
	
	public Product(int id, String name, String description, String availability, double price, int nbVente, int rank) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.availability = availability;
		this.price = price;
		this.nbVente = nbVente;
		this.rank = rank;
	}

	public Product(String name, String description, String availability, double price, int nbVente, int rank) {
		this.name = name;
		this.description = description;
		this.availability = availability;
		this.price = price;
		this.nbVente = nbVente;
		this.rank = rank;
	}
	
	public Product() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getavailability() {
		return availability;
	}

	public void setavailability(String availability) {
		this.availability = availability;
	}

	public double getprice() {
		return price;
	}

	public void setprice(double price) {
		this.price = price;
	}

	public int getNbVente() {
		return nbVente;
	}

	public void setNbVente(int nbVente) {
		this.nbVente = nbVente;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", availability="
				+ availability + ", price=" + price + ", nbVente=" + nbVente + ", rank=" + rank + "]";
	}

	
	
	
	
	
}
