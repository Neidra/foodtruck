package ws;

import java.util.List;

import javax.jws.*;

import dao.*;
import entities.Product;


@WebService(endpointInterface = "ws.IProduct")
public class ProductImpl implements IProduct {
	private DAOProduct DAO = new DAOProduct();
	
	public ProductImpl(DAOProduct dAO) {
		super();
		this.DAO = dAO;
	}


	public Product findById(int uId) {
		try {
			return this.DAO.findById(uId);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
		
		//return new Product(1, "Plop", "Cool", 105);	
	}
	
	public List<Product>findAll() {
		// TODO Auto-generated method stub
		try {
			return this.DAO.findAll();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<Product> findThreeBestSales(){
		try {
			return this.DAO.findThreeBestSales();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	



}
