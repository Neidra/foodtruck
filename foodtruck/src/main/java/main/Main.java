package main;

import javax.xml.ws.*;

import dao.DAOProduct;
import ws.NewsImpl;
import ws.ProductImpl;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DAOProduct d = new DAOProduct();
		try {
			
//			Endpoint.publish("http://localhost:4794/ws/News", new NewsImpl());
			Endpoint.publish("http://localhost:4815/ws/IProduct", new ProductImpl(d));
			System.out.println("Done");
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}

}
