package entities;

public class Product {

	
	private int id;
	private String nom, description, disponibilite;
	private double prix;
	private int nbVente;
	
	public Product(int id, String nom, String description, String disponibilite, double prix, int nbVente) {
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.disponibilite = disponibilite;
		this.prix = prix;
		this.nbVente = nbVente;
	}

	public Product(String nom, String description, String disponibilite, double prix, int nbVente) {
		this.nom = nom;
		this.description = description;
		this.disponibilite = disponibilite;
		this.prix = prix;
		this.nbVente = nbVente;
	}
	
	public Product() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getnom() {
		return nom;
	}

	public void setnom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getNbVente() {
		return nbVente;
	}

	public void setNbVente(int nbVente) {
		this.nbVente = nbVente;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", nom=" + nom + ", description=" + description + ", disponibilite="
				+ disponibilite + ", prix=" + prix + ", nbVente=" + nbVente + "]";
	}
	
	
	
	
}
