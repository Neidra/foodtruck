package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import dao.DAONews;
import entities.News;
import entities.Product;

@WebService(endpointInterface = "ws.INews")
public class NewsImpl {
	
	private DAONews dao = new DAONews();
	
	public NewsImpl(DAONews dao) {
		this.dao = dao;
	}


	public News findById(int id) {
		return this.dao.findById(id);
	}
	
	
	public List<News> findAll() {
		return this.dao.findAll();
	}
	
	
	public List<News> findLastThree() {
		return this.dao.findLastThree();
	}
}
