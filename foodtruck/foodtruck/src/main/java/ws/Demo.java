package ws;

import javax.jws.*;

@WebService
public interface Demo {

	@WebMethod
	public void findById();
	
}
