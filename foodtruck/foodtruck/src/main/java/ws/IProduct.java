package ws;

import entities.Product;

import javax.jws.*;

import java.util.List;

@WebService
public interface IProduct {

	@WebMethod
	Product findById(int id);
	
	@WebMethod
	List<Product> findAll();
	
}