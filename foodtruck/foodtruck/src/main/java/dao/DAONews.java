package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import entities.News;

public class DAONews implements DAO<News, Integer> {
	
	static private String DBHOST = "jdbc:mysql://localhost:3306/foodtruck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";

	private List<News> ps = new ArrayList<News>();
	
	
	public News findById(Integer uId) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			
			PreparedStatement ps=conn.prepareStatement("select * from news where id=?");
			ps.setInt(1,uId);
			ResultSet rs=ps.executeQuery();
			News p=null;
			while(rs.next())
			{
				p=new News(rs.getInt("id"),rs.getString("title"),rs.getString("content"),rs.getString("image"));
			}
			rs.close();
			ps.close();
			conn.close();
			return p;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<News> findAll() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			PreparedStatement ps=conn.prepareStatement("select * from news");
			ResultSet rs=ps.executeQuery();
			List<News> liste=new ArrayList();
			while(rs.next())
			{
				News p=new News(rs.getInt("id"),rs.getString("title"),rs.getString("content"),rs.getString("image"));
				liste.add(p);
			}
			rs.close();
			ps.close();
			conn.close();
			return liste;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}
	
	public List<News> findLastThree() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection(DBHOST, DBUSER , DBPSWD );
			PreparedStatement ps=conn.prepareStatement("select * from news order by id desc limit 3");
			ResultSet rs=ps.executeQuery();
			List<News> liste=new ArrayList();
			while(rs.next())
			{
				News p=new News(rs.getInt("id"),rs.getString("title"),rs.getString("content"),rs.getString("image"));
				liste.add(p);
			}
			rs.close();
			ps.close();
			conn.close();
			return liste;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
		
	}


}
